FROM node:16-alpine

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN yarn install

CMD ["node", "dist/main"]

EXPOSE 3000
