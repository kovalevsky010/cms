require('dotenv').config();
import { ConnectionOptions } from 'typeorm';

const port = parseInt(process.env.POSTGRES_PORT) || 6432;

const DatabaseConnectionTestConfiguration: ConnectionOptions = {
  type: 'postgres',
  host: 'localhost',
  port: port,
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DB,
  synchronize: true,
  dropSchema: true,
  logging: true,
};

module.exports = {
  entities: ['dist/**/*.entity.js'],
  factories: [ "dist/**/database/factories/**/*.js" ],
  seeds: [ "dist/**/database/seeds/**/*.js" ],
  ...DatabaseConnectionTestConfiguration,
}
