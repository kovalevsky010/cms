import { Body, Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { AuthService } from './services/auth.service';
import { SignInDto } from './dto/sign-in.dto';
import { SignUpResponseDto } from './dto/sign-up-response.dto';
import { SignUpDto } from './dto/sign-up.dto';
import { SignInResponseDto } from './dto/sing-in-response.dto'
import { ApiBearerAuth, ApiBody, ApiCreatedResponse, ApiOkResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { Public } from '../common/decorators/public.decorator';
import { AccessRefreshTokensDto } from './dto/access-refresh-tokens.dto';
import { JwtRefreshAuthGuard } from './guards/jwt-refresh.guard';
import { Self } from 'src/common/decorators/self.decorator';
import { User } from 'src/users/entities/user.entity';
import { ApiProperty } from '@nestjsx/crud/lib/crud';
import { Request } from 'express';
import { GoogleAuthGuard } from './guards/google-auth.guard';


@ApiTags('auth')
@ApiBearerAuth('JWT')
@ApiUnauthorizedResponse({
  description: 'Unauthorized',
})
@Controller('/auth')
export class AuthController {
  constructor(private readonly authService: AuthService) { }
  
  // google auth
  @Public()
  @UseGuards(GoogleAuthGuard)
  @Get('/google')
  async googleAuth(@Req() req: Request) {}

  @Public()
  @UseGuards(GoogleAuthGuard)
  @Get('/google/signIn')
  async googleSignUp(@Self() user: User) {
    return this.authService.googleSingIn(user);
  }
  
  // jwt auth with refresh token
  @Public()
  @ApiBody({type: SignUpDto})
  @ApiCreatedResponse({
    description: 'User was authenticated',
    type: SignUpResponseDto
  })
  @Post('signUp')
  async signUp(@Body() dto: SignUpDto): Promise<SignUpResponseDto> {
    return this.authService.signUp(dto);
  }

  @Public()
  @ApiBody({type: SignInDto})
  @ApiCreatedResponse({
    description: 'User was authenticated',
    type: SignInResponseDto,
  })
  @Post('signIn')
  async singIn(@Body() dto: SignInDto): Promise<SignInResponseDto> {
    return this.authService.signIn(dto);
  }

  @ApiProperty() 
  @ApiOkResponse({
    type: Boolean,
    description: 'logout',
  })
  @Post('logout')
  async logout(@Self('id') userId: User['id']): Promise<boolean> {
    console.log(userId);
    return this.authService.logout(userId);
  }

  @Public()
  @UseGuards(JwtRefreshAuthGuard)
  @ApiProperty()
  @ApiOkResponse({
    type: AccessRefreshTokensDto,
    description: 'refresh tokens',
  })
  @Post('refresh')
  async rehresh(
    @Self('refreshToken') refreshToken: string,
    @Self('sub') userId: User['id'],
  ): Promise<AccessRefreshTokensDto> {
    return this.authService.refresh(userId, refreshToken);
  }
}
