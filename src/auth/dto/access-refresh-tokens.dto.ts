export class AccessRefreshTokensDto {
  accessToken: string;
  
  refreshToken: string;
}
