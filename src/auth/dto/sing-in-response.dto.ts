import { ApiProperty } from "@nestjsx/crud/lib/crud";
import { User } from "src/users/entities/user.entity";

export class SignInResponseDto {
  @ApiProperty()
  accessToken: string;

  @ApiProperty()
  refreshToken: string;

  @ApiProperty()
  user: User;
}
