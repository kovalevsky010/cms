import { IsEmail, IsNumber } from "class-validator";

export class JwtPayload {
  sub: number;

  email: string;
}
