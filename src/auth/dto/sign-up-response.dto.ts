import { ApiProperty } from "@nestjsx/crud/lib/crud";
import { IsDefined, IsString } from "class-validator";
import { User } from "src/users/entities/user.entity";

export class SignUpResponseDto {
  @ApiProperty()
  @IsString()
  @IsDefined()
  accessToken: string;

  @ApiProperty()
  @IsString()
  @IsDefined()
  refreshToken: string;

  @IsDefined()
  @ApiProperty()
  user: User;
}
