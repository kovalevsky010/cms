import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy, StrategyOptions, VerifyCallback } from "passport-google-oauth20";
import { User } from "src/users/entities/user.entity";
import { HashService } from "src/users/services/hash.service";
import { UsersRepository } from "src/users/users.repository";
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {
  constructor(
    private readonly usersRepository: UsersRepository,
    private readonly hashService: HashService,
    private readonly configService: ConfigService,
  ) {
    super({
      clientID: configService.get<string>('GOOGLE_AUTH_CLIENT_ID'),
      clientSecret: configService.get<string>('GOOGLE_AUTH_CLIENT_SECRET'),
      callbackURL: configService.get<string>('GOOGLE_AUTH_CALLBACK_URL'),
      scope: ['email', 'profile'],
    })
  }

  async validate(
    accessToken: string,
    refreshToken: string,
    profile: any,
    done: VerifyCallback
  ) {
    const { emails } = profile
    const email = emails[0].value;

    let user = await this.usersRepository.findOne({
      where: { email }
    });

    if (!user) {
      user = await this.usersRepository.save({
        email,
        password: await this.hashService.hash(uuidv4()),
      });
    }

    const userDataForRequest = {
      id: user.id,
      email: user.email
    }

    done(null, userDataForRequest);
  }
}