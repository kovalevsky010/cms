import { ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { plainToClass } from 'class-transformer';
import { User } from 'src/users/entities/user.entity';
import { HashService } from 'src/users/services/hash.service';
import { UsersRepository } from 'src/users/users.repository';
import { AccessRefreshTokensDto } from '../dto/access-refresh-tokens.dto';
import { SignInDto } from '../dto/sign-in.dto';
import { SignUpResponseDto } from '../dto/sign-up-response.dto';
import { SignUpDto } from '../dto/sign-up.dto';
import { SignInResponseDto } from '../dto/sing-in-response.dto';


@Injectable()
export class AuthService {
  constructor(
    private readonly usersRepository: UsersRepository,
    private readonly hashService: HashService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) {}
  
  async googleSingIn(user: User): Promise<SignInResponseDto> {
    console.log('user: >>', user);
    const tokens = await this.issuePairTokens({
      sub: user.id,
      email: user.email
    });

    await this.usersRepository.save({
      ...user,
      currentRefreshToken: tokens.refreshToken
    });

    return {
      ...tokens,
      user: plainToClass(User, user)
    }
  }
  
  async issuePairTokens(
    payload: { sub: number, email: string }
  ): Promise<AccessRefreshTokensDto> {
    const [ accessToken, refreshToken ] = await Promise.all([
      this.jwtService.signAsync(payload, {
        secret: this.configService.get<'string'>('JWT_ACCESS_SECRET_KEY'),
        expiresIn: '15m',
      }),
      this.jwtService.signAsync(payload, {
        secret: this.configService.get<'string'>('JWT_REFRESH_SECRET_KEY'),
        expiresIn: '7d',
      })
    ]);
    
    return {
      accessToken,
      refreshToken
    }
  }
 
  async signUp(dto: SignUpDto): Promise<SignUpResponseDto> {
    const isUserAlreadyExists = await this.usersRepository.count({
      where: { email: dto.email },
    });

    if (isUserAlreadyExists > 0) {
      throw new ForbiddenException('User with this email already exist.');
    }

    const newUser = await this.usersRepository.create({
      ...dto,
      password: await this.hashService.hash(dto.password)
    });

    const payload = { 
      sub: newUser.id,
      email: newUser.email
    }

    const { accessToken, refreshToken } = await this.issuePairTokens(payload);

    await this.usersRepository.save({
      ...newUser,
      currentRefreshToken: refreshToken
    })

    return {
      accessToken: accessToken,
      refreshToken: refreshToken,
      user: plainToClass(User, newUser),
    }
  }

  async signIn(dto: SignInDto): Promise<SignInResponseDto> {
    const user = await this.usersRepository.findOne({
      where: { email: dto.email }
    });

    const isPosswordCorrect = await this.hashService.compare(
      dto.password,
      user.password
    );

    if (!user || !isPosswordCorrect) {
      throw new NotFoundException('Wrong email or password.');
    }

    const payload = { 
      sub: user.id,
      email: user.email
    }

    const { accessToken, refreshToken } = await this.issuePairTokens(payload);

    await this.usersRepository.save({
      ...user,
      currentRefreshToken: refreshToken
    })

    return {
      accessToken: accessToken,
      refreshToken: refreshToken,
      user: plainToClass(User, user),
    }
  }

  async logout(userId: User['id']): Promise<boolean> {
    await this.usersRepository.save({
      id: userId,
      currentRefreshToken: null,
    });

    return true;
  }

  async refresh(
    userId: User['id'],
    refreshToken: string
  ): Promise<AccessRefreshTokensDto> {
    const user = await this.usersRepository.findOneOrFail(userId);

    const isTokensMatches = refreshToken === user.currentRefreshToken;

    if (!user || !isTokensMatches) {
      throw new ForbiddenException('Access Denied');
    }

    const tokens = await this.issuePairTokens({
      sub: user.id,
      email: user.email
    });

    await this.usersRepository.save({
      ...user,
      currentRefreshToken: tokens.refreshToken
    })

    return tokens;
  }
}
