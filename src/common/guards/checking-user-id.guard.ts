import { CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import { User } from "src/users/entities/user.entity";
import { EntityTarget, getRepository, Repository } from "typeorm";

@Injectable()
export class CheckingUserIdGuard<Entity extends {userId: User['id']}>
  implements CanActivate
{
  constructor(
    private entity: EntityTarget<Entity>, 
    private paramName: string = 'id'
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const params = request.params;
    const user = request.user;
    
    const repository: Repository<any> = await getRepository(this.entity);
    const checkedEntity = await repository.findOne(params[this.paramName]);
    
    return user.id === checkedEntity.userId;
  }
}
