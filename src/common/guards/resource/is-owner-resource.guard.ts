import { Content } from "src/content/entities/content.entity";
import { Event } from "src/events/entities/event.entity";
import { Playlist } from "src/playlists/entities/playlist.entity";
import { Screen } from "src/screens/entities/screen.entity";
import { User } from "src/users/entities/user.entity";
import { CheckingUserIdGuard } from "../checking-user-id.guard";

export const IsOwnerEventResourceGuard = new CheckingUserIdGuard(Event);
export const IsOwnerScreenResourceGuard = new CheckingUserIdGuard(Screen);
export const IsOwnerPlaylistResourceGuard = new CheckingUserIdGuard(Playlist);
export const IsOwnerUserResourceGuard = new CheckingUserIdGuard(User);
export const IsOwnerContentResourceGuard = new CheckingUserIdGuard(Content);
