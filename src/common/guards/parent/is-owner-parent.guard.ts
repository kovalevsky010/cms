import { Content } from "src/content/entities/content.entity";
import { Event } from "src/events/entities/event.entity";
import { Playlist } from "src/playlists/entities/playlist.entity";
import { Screen } from "src/screens/entities/screen.entity";
import { CheckingUserIdGuard } from "../checking-user-id.guard";

export const IsOwnerEventGuard = new CheckingUserIdGuard(Event, 'eventId');
export const IsOwnerScreenGuard = new CheckingUserIdGuard(Screen, 'screenId');
export const IsOwnerPlaylistGuard = new CheckingUserIdGuard(Playlist, 'playlistId');
export const IsOwnerContentGuard = new CheckingUserIdGuard(Content, 'contentId');
