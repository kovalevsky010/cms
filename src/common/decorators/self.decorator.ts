import { createParamDecorator, ExecutionContext } from "@nestjs/common";
import { userInfo } from "os";

export const Self = createParamDecorator(
  (property: string | undefined, context: ExecutionContext) => {
    const request = context.switchToHttp().getRequest();

    if (!property) {
      return request.user;
    }

    return request.user[property];
  }
);
