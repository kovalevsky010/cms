import { ApiProperty } from "@nestjsx/crud/lib/crud";
import { IsDateString, IsNumber } from "class-validator";
import { CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

export class Base {
  @ApiProperty()
  @IsNumber()
  @PrimaryGeneratedColumn()
  id: number

  @ApiProperty()
  @IsDateString()
  @CreateDateColumn()
  createdAt: string;

  @ApiProperty()
  @IsDateString()
  @UpdateDateColumn()
  updatedAt: string;
}
