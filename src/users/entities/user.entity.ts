import { Base } from "src/common/entities/base.entity"
import { Content } from "src/content/entities/content.entity";
import { Event } from "src/events/entities/event.entity";
import { Playlist } from "src/playlists/entities/playlist.entity";
import { Screen } from "src/screens/entities/screen.entity";
import { BeforeInsert, Column, Entity, OneToMany } from "typeorm"
import * as bcrypt from 'bcrypt';
import { Exclude } from "class-transformer";

@Entity({ name: 'users' })
export class User extends Base {
  @Column('varchar', { unique: true })
  email: string;

  @Exclude({ toPlainOnly: true })
  @Column('varchar')
  password: string;

  @Exclude({ toPlainOnly: true })
  @Column('varchar', { nullable: true })
  currentRefreshToken?: string;

  // relations
  @OneToMany(() => Event, event => event.user)
  events: Event[];

  @OneToMany(() => Screen, screen => screen.user)
  screens: Screen[];

  @OneToMany(() => Content, content => content.user)
  content: Content[];

  @OneToMany(() => Playlist, playlist => playlist.user)
  playlists: Playlist[];

}
