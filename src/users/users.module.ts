import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Content } from 'src/content/entities/content.entity';
import { Event } from 'src/events/entities/event.entity';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { Screen } from 'src/screens/entities/screen.entity';
import { User } from './entities/user.entity';
import { UsersController } from './users.controller';
import { UsersRepository } from './users.repository';
import { UsersService } from './services/users.service';
import { HashService } from './services/hash.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      User, 
      Event, 
      Screen, 
      Playlist,
      Content,
      UsersRepository,
    ])
  ],
  controllers: [UsersController],
  providers: [UsersService, HashService],
  exports: [UsersService, HashService]
})
export class UsersModule {}
