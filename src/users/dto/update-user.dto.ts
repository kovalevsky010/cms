import { ApiProperty } from "@nestjsx/crud/lib/crud";
import { IsEmail, IsOptional, IsString, Length } from "class-validator";


export class UpdateUserDto {
  @ApiProperty({
    example: 'user@gmail.com',
  })
  @IsEmail()
  @IsOptional()
  email?: string;

  @ApiProperty({
    example: '1234UseR',
    minLength: 8,
    maxLength: 100
  })
  @Length(8, 100)
  @IsString()
  @IsOptional()
  password?: string;
}
