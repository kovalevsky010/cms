import { ApiProperty } from "@nestjsx/crud/lib/crud";
import { IsDefined, IsEmail, IsString, Length } from "class-validator";


export class CreateUserDto {
  @ApiProperty({
    example: 'user@gmail.com',
  })
  @IsEmail()
  @IsDefined()
  email: string;

  @ApiProperty({
    example: '1234UseR',
    minLength: 8,
    maxLength: 100
  })
  @Length(8, 100)
  @IsString()
  @IsDefined()
  password: string;
}
