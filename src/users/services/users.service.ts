import { Injectable } from '@nestjs/common';
import { CrudRequest } from '@nestjsx/crud';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { CreateUserDto } from '../dto/create-user.dto';
import { UpdateUserDto } from '../dto/update-user.dto';
import { User } from '../entities/user.entity';
import { UsersRepository } from '../users.repository';
import { HashService } from './hash.service';


@Injectable()
export class UsersService extends TypeOrmCrudService<User> {
  constructor(
    private readonly usersRepository: UsersRepository,
    private readonly hashService: HashService
  ) {
    super(usersRepository)
  }

  async updateOne(request: CrudRequest, dto: UpdateUserDto): Promise<User> {
    if (dto.password) {
      dto.password = await this.hashService.hash(dto.password);
    }
    return super.updateOne(request, dto);
  }
  
  async registerOne(dto: CreateUserDto): Promise<User> {
    dto.password = await this.hashService.hash(dto.password);
    return this.usersRepository.save(dto);
  }
}
