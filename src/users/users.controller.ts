import { Controller } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Crud, CrudAuth, CrudController } from '@nestjsx/crud';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { UsersService } from './services/users.service';


@ApiTags('users')
@ApiBearerAuth('JWT')
@Crud({
  model: {
    type: User,
  },
  dto: {
    create: CreateUserDto,
    update: UpdateUserDto,
  },
  routes: {
    exclude: [
      'createOneBase',
      'createManyBase', 
      'replaceOneBase',
      'recoverOneBase'
    ],
  }
})
@CrudAuth({
  property: 'user',
  filter: (user: User) => ({
    id: user.id
  }),
})
@Controller('/users')
export class UsersController implements CrudController<User> {
  constructor(public service: UsersService) {}
}
