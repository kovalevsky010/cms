import { ApiProperty } from "@nestjsx/crud/lib/crud";
import { IsDefined, IsNotEmpty, IsString, Length } from "class-validator";


export class CreatePlaylistDto {
  @ApiProperty({
    example: 'my playlist name',
    minLength: 3,
    maxLength: 100
  })
  @Length(3, 100)
  @IsNotEmpty()
  @IsString()
  @IsDefined()
  name: string;
}
