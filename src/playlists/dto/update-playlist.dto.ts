import { ApiProperty } from "@nestjsx/crud/lib/crud";
import { IsNotEmpty, IsOptional, IsString, Length } from "class-validator";


export class UpdatePlaylistDto {
  @ApiProperty({
    example: 'my playlist name',
    minLength: 3,
    maxLength: 100
  })
  @Length(3, 100)
  @IsNotEmpty()
  @IsString()
  @IsOptional()
  name?: string;
}
