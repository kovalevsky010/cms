import { Module } from '@nestjs/common';
import { PlaylistsService } from './playlists.service';
import { PlaylistsController } from './playlists.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User} from 'src/users/entities/user.entity';
import { Playlist } from './entities/playlist.entity';
import { PlaylistContent } from 'src/playlist-content/entities/playlist-content.entity';
import { Content } from 'src/content/entities/content.entity';
import { PlaylistRepository } from './playlists.repository';


@Module({
  imports: [
    TypeOrmModule.forFeature([
      Playlist,
      PlaylistContent,
      User,
      Content,
      PlaylistRepository,
    ])
  ],
  providers: [PlaylistsService],
  controllers: [PlaylistsController]
})
export class PlaylistsModule {}
