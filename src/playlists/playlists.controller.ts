import { Controller, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Crud, CrudAuth, CrudController } from '@nestjsx/crud';
import { IsOwnerScreenGuard } from 'src/common/guards/parent/is-owner-parent.guard';
import { IsOwnerPlaylistResourceGuard } from 'src/common/guards/resource/is-owner-resource.guard';
import { User } from 'src/users/entities/user.entity';
import { CreatePlaylistDto } from './dto/create-playlist.dto';
import { UpdatePlaylistDto } from './dto/update-playlist.dto';
import { Playlist } from './entities/playlist.entity';
import { PlaylistsService } from './playlists.service';

@ApiTags('playlists')
@ApiBearerAuth('JWT')
@Crud({
  model: {
    type: Playlist,
  },
  params: {
    screenId: {
      field: 'screenId',
      type: 'number',
    }
  },
  dto: {
    create: CreatePlaylistDto,
    update: UpdatePlaylistDto,
  },
  routes: {
    exclude: [
      'recoverOneBase', 
      'replaceOneBase'
    ],
    createOneBase: {
      decorators: [UseGuards(IsOwnerScreenGuard)]
    },
    createManyBase: {
      decorators: [UseGuards(IsOwnerScreenGuard)]
    },
    updateOneBase: {
      decorators: [UseGuards(IsOwnerPlaylistResourceGuard)]
    },
    deleteOneBase: {
      decorators: [UseGuards(IsOwnerPlaylistResourceGuard)]
    }
  },
})
@CrudAuth({
  property: 'user',
  persist: (user: User) => ({
    userId: user.id
  }),
})
@Controller('/screens/:screenId/playlist')
export class PlaylistsController implements CrudController<Playlist> {
  constructor(public service: PlaylistsService) {}
}
