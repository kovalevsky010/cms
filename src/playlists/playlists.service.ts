import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Playlist } from './entities/playlist.entity';
import { PlaylistRepository } from './playlists.repository';

@Injectable()
export class PlaylistsService extends TypeOrmCrudService<Playlist> {
  constructor(
    private readonly playlistsRepository: PlaylistRepository,
  ) { 
    super(playlistsRepository);
  }
}
