import { EntityRepository, Repository } from "typeorm";
import { Playlist } from "./entities/playlist.entity";

@EntityRepository(Playlist)
export class PlaylistRepository extends Repository<Playlist> {}
