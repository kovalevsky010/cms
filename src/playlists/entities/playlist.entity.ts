import { Base } from "src/common/entities/base.entity";
import { PlaylistContent } from "src/playlist-content/entities/playlist-content.entity";
import { Screen } from "src/screens/entities/screen.entity";
import { User } from "src/users/entities/user.entity";
import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne } from "typeorm";


@Entity({name: 'playlists'})
export class Playlist extends Base {
  @Column()
  name: string;

  @Column()
  screenId: Screen['id'];

  @Column()
  userId: User['id'];

  // relations
  @OneToOne(() => Screen)
  @JoinColumn()
  screen: Screen;

  @ManyToOne(() => User, user => user.playlists, {
    onDelete: 'CASCADE' 
  })
  user: User;

  @OneToMany(
    () => PlaylistContent, 
    playlistContent => playlistContent.playlist
  )
  playlistContent: PlaylistContent[];
}
