import { define } from 'typeorm-seeding'
import { Faker } from '@faker-js/faker'
import { getRandomEnumValue } from 'src/utils/utils';
import { ContentType } from 'src/content/content.types';
import { Content } from 'src/content/entities/content.entity';

define(Content, (faker: Faker) => {
  const content = new Content();
  
  content.source = faker.internet.url();
  content.contentType = getRandomEnumValue(ContentType);

  return content;
})