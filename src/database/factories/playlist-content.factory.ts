import { define } from 'typeorm-seeding'
import { Faker } from '@faker-js/faker'
import { PlaylistContent } from 'src/playlist-content/entities/playlist-content.entity';

define(PlaylistContent, (facker: Faker) => {
  const playlistContent = new PlaylistContent();

  playlistContent.duration = facker.random.number(10)
  playlistContent.ordinalNumber = facker.random.number(10)

  return playlistContent;
})