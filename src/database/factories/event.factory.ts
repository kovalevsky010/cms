import { define, factory } from 'typeorm-seeding'
import { User } from "src/users/entities/user.entity";
import { Faker } from '@faker-js/faker'
import { Event } from 'src/events/entities/event.entity';

define(Event, (faker: Faker) => {
  const event = new Event();

  event.name = faker.lorem.word();
  event.startDate = new Date();
  event.endDate = new Date();

  event.user = factory(User)() as any;

  return event;
})