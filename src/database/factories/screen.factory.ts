import { define } from 'typeorm-seeding'
import { Faker } from '@faker-js/faker'
import { Screen } from 'src/screens/entities/screen.entity';

define(Screen, (faker: Faker) => {
  const screen = new Screen();

  screen.name = faker.lorem.word();

  return screen;
})
