import { define } from 'typeorm-seeding'
import { Playlist } from "src/playlists/entities/playlist.entity";
import { Faker } from '@faker-js/faker'

define(Playlist, (facker: Faker) => {
  const playlist = new Playlist();

  playlist.name = facker.lorem.word();

  return playlist;
})