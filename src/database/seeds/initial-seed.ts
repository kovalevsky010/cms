import { Content } from 'src/content/entities/content.entity';
import { Event } from 'src/events/entities/event.entity';
import { PlaylistContent } from 'src/playlist-content/entities/playlist-content.entity';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { Screen } from 'src/screens/entities/screen.entity';
import { Factory, Seeder } from 'typeorm-seeding'


export default class InitialSeed implements Seeder {
  public async run(factory: Factory): Promise<any> {
    const events: Event[] = await factory(Event)().createMany(5);

    const screens: Screen[] = await Promise.all(
      events.map((event) => {
        return factory(Screen)().create({
          event: event,
          user: event.user
        })
      })
    );

    const playlists: Playlist[] = await Promise.all(
      screens.map((screen) => {
        return factory(Playlist)().create({
          screen: screen,
          user: screen.user,
        })
      })
    );


    playlists.map(async (playlist) => {
      const content: Content = await factory(Content)().create({
        user: playlist.user
      })
      await factory(PlaylistContent)().create({
        content: content,
        playlist: playlist
      })
    });
  
  }
}
