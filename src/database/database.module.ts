import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Base } from 'src/common/entities/base.entity';
import { Content } from 'src/content/entities/content.entity';
import { Event } from 'src/events/entities/event.entity';
import { PlaylistContent } from 'src/playlist-content/entities/playlist-content.entity';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { Screen } from 'src/screens/entities/screen.entity';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      name: 'default',
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        type: 'postgres',
        host: configService.get('POSTGRES_HOST'),
        port: configService.get('POSTGRES_PORT'),
        username: configService.get('POSTGRES_USER'),
        password: configService.get('POSTGRES_PASSWORD'),
        database: configService.get('POSTGRES_DB'),
        entities: [
          Base,
          User,
          Event,
          Screen,
          Playlist, 
          Content, 
          PlaylistContent,
        ],
        synchronize: true,
      })
    })
  ]
})
export class DatabaseModule {}
