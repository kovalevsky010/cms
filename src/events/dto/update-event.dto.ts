import { ApiProperty } from "@nestjsx/crud/lib/crud";
import { IsDateString, IsOptional, IsString, Length } from "class-validator";


export class UpdateEventDto {
  @ApiProperty({
    example: 'event',
    minLength: 3,
    maxLength: 100,
  })
  @Length(3, 100)
  @IsString()
  @IsOptional()
  name?: string;

  @ApiProperty()
  @IsDateString()
  @IsOptional()
  startDate?: Date;

  @ApiProperty()
  @IsDateString()
  @IsOptional()
  endDate?: Date;
}
