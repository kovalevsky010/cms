import { ApiProperty } from "@nestjsx/crud/lib/crud";
import { IsDateString, IsDefined, IsString, Length } from "class-validator";


export class CreateEventDto {
  @ApiProperty({
    example: 'event',
    minLength: 3,
    maxLength: 100,
  })
  @Length(3, 100)
  @IsString()
  @IsDefined()
  name: string;

  @ApiProperty()
  @IsDateString()
  @IsDefined()
  startDate: Date;

  @ApiProperty()
  @IsDateString()
  @IsDefined()
  endDate: Date;
}
