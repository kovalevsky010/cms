import { Controller } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Crud, CrudAuth, CrudController } from '@nestjsx/crud';
import { User } from 'src/users/entities/user.entity';
import { CreateEventDto } from './dto/create-event.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { Event } from './entities/event.entity';
import { EventsService } from './events.service';

@ApiTags('events')
@ApiBearerAuth('JWT')
@Crud({
  model: {
    type: Event,
  },
  dto: {
    create: CreateEventDto,
    update: UpdateEventDto,
  },
  routes: {
    exclude: [
      'recoverOneBase', 
      'replaceOneBase'
    ],
  }
})
@CrudAuth({
  property: 'user',
  persist: (user: User) => ({
    userId: user.id
  }),
  filter: (user: User) => ({
    userId: user.id
  }),
})
@Controller('/events')
export class EventsController implements CrudController<Event> {
  constructor(public service: EventsService) {}
}
