import { Module } from '@nestjs/common';
import { EventsService } from './events.service';
import { EventsController } from './events.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User} from 'src/users/entities/user.entity';
import { Event } from './entities/event.entity';
import { Screen } from 'src/screens/entities/screen.entity';
import { EventsRepository } from './events.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Event,
      User,
      Screen,
      EventsRepository
    ])
  ],
  providers: [EventsService],
  controllers: [EventsController]
})
export class EventsModule {}
