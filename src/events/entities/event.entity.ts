import { Base } from "src/common/entities/base.entity"
import { Screen } from "src/screens/entities/screen.entity";
import { User } from "src/users/entities/user.entity";
import { Column, Entity, ManyToOne, OneToMany } from "typeorm"


@Entity({ name: 'events' })
export class Event extends Base {
  @Column('varchar')
  name: string;

  @Column({type: 'timestamp'})
  startDate: Date;

  @Column({type: 'timestamp'})
  endDate: Date;

  @Column()
  userId: User['id'];

  // relations
  @ManyToOne(() => User, user => user.events, {
    onDelete: 'CASCADE'
  })
  user: User;

  @OneToMany(() => Screen, screen => screen.event)
  screens: Screen[];
}
