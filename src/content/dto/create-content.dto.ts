import { ApiProperty } from "@nestjsx/crud/lib/crud";
import { IsDefined, IsEnum, IsString, Length } from "class-validator";
import { ContentType } from "../content.types";


export class CreateContentDto {
  @ApiProperty({
    example: 'my content name',
    minLength: 3,
    maxLength: 100
  })
  @Length(3, 100)
  @IsString()
  @IsDefined()
  source: string;

  @ApiProperty({
    example: 'html',
    enum: ContentType
  })
  @IsEnum(ContentType)
  @IsDefined()
  contentType: ContentType;
}
