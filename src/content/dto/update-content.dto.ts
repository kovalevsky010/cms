import { ApiProperty } from "@nestjsx/crud/lib/crud";
import { IsEnum, IsOptional, IsString, Length } from "class-validator";
import { ContentType } from "../content.types";


export class UpdateContentDto {
  @ApiProperty({
    example: 'my content name',
    minLength: 3,
    maxLength: 100
  })
  @Length(3, 100)
  @IsString()
  @IsOptional()
  source?: string;

  @ApiProperty({
    example: 'html',
    enum: ContentType
  })
  @IsEnum(ContentType)
  @IsOptional()
  contentType?: ContentType;
}
