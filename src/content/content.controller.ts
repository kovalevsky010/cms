import { Controller } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Crud, CrudAuth, CrudController } from '@nestjsx/crud';
import { User } from 'src/users/entities/user.entity';
import { Content } from './entities/content.entity';
import { ContentService } from './content.service';
import { CreateContentDto } from './dto/create-content.dto';
import { UpdateContentDto } from './dto/update-content.dto';

@ApiTags('content')
@ApiBearerAuth('JWT')
@Crud({
  model: {
    type: Content,
  },
  params: {
    userId: {
      field: 'userId',
      type: 'number',
    }
  },
  dto: {
    create: CreateContentDto,
    update: UpdateContentDto,
  },
  routes: {
    exclude: [
      'recoverOneBase', 
      'replaceOneBase'
    ],
  }
})
@CrudAuth({
  property: 'user',
  persist: (user: User) => ({
    userId: user.id
  }),
  filter: (user: User) => ({
    userId: user.id
  }),
})
@Controller('/content')
export class ContentController implements CrudController<Content> {
  constructor(public service: ContentService) {}
}
