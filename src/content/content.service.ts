import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { ContentRepository } from './content.repository';
import { Content } from './entities/content.entity';


@Injectable()
export class ContentService extends TypeOrmCrudService<Content> {
  constructor(
    private readonly contentRepository: ContentRepository
  ) { 
    super(contentRepository);
  }
}
