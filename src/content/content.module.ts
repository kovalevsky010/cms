import { Module } from '@nestjs/common';
import { ContentService } from './content.service';
import { ContentController } from './content.controller';
import { User } from 'src/users/entities/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Content } from './entities/content.entity';
import { PlaylistContent } from 'src/playlist-content/entities/playlist-content.entity';
import { ContentRepository } from './content.repository';


@Module({
  imports: [
    TypeOrmModule.forFeature([
      Content,
      User,
      PlaylistContent,
      ContentRepository,
    ]),
  ],
  providers: [ContentService],
  controllers: [ContentController]
})
export class ContentModule {}
