export enum ContentType {
  video = 'video',
  audio = 'audio',
  image = 'image',
  html = 'html',
}
