import { Base } from "src/common/entities/base.entity"
import { PlaylistContent } from "src/playlist-content/entities/playlist-content.entity";
import { User } from "src/users/entities/user.entity";
import { Column, Entity, ManyToOne, OneToMany } from "typeorm"
import { ContentType } from "../content.types";


@Entity({ name: 'content' })
export class Content extends Base {
  @Column('varchar')
  source: string;

  @Column()
  contentType: ContentType;

  @Column()
  userId: User['id'];

  // relations
  @ManyToOne(() => User, user => user.content, {
    onDelete: 'CASCADE'
  })
  user: User;

  @OneToMany(
    () => PlaylistContent,
    playlistContent => playlistContent.content
  )
  playlistContent: PlaylistContent[];
}
