import { ApiProperty } from "@nestjsx/crud/lib/crud";
import { IsDefined, IsString, Length } from "class-validator";


export class CreateScreenDto {
  @ApiProperty({
    example: 'my screen name',
    minLength: 3,
    maxLength: 100
  })
  @Length(3, 100)
  @IsString()
  @IsDefined()
  name: string;
}
