import { ApiProperty } from "@nestjsx/crud/lib/crud";
import { IsOptional, IsString, Length } from "class-validator";


export class UpdateScreenDto {
  @ApiProperty({
    example: 'my screen name',
    minLength: 3,
    maxLength: 100
  })
  @Length(3, 100)
  @IsString()
  @IsOptional()
  name?: string;
}

