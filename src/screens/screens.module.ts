import { Module } from '@nestjs/common';
import { ScreensService } from './screens.service';
import { ScreensController } from './screens.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Screen} from './entities/screen.entity';
import { User} from 'src/users/entities/user.entity';
import { Event } from 'src/events/entities/event.entity';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { ScreensRepository } from './screens.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Screen,
      User,
      Event,
      Playlist,
      ScreensRepository,
    ])
  ],
  providers: [ScreensService],
  controllers: [ScreensController]
})
export class ScreensModule {}
