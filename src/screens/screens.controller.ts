import { Controller, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Crud, CrudAuth, CrudController } from '@nestjsx/crud';
import { IsOwnerEventGuard } from 'src/common/guards/parent/is-owner-parent.guard';
import { IsOwnerScreenResourceGuard } from 'src/common/guards/resource/is-owner-resource.guard';
import { User } from 'src/users/entities/user.entity';
import { CreateScreenDto } from './dto/create-screen.dto';
import { UpdateScreenDto } from './dto/update-screen.dto';
import { Screen } from './entities/screen.entity';
import { ScreensService } from './screens.service';


@ApiTags('screens')
@ApiBearerAuth('JWT')
@UseGuards(IsOwnerEventGuard)
@Crud({
  model: {
    type: Screen,
  },
  params: {
    eventId: {
      field: 'eventId',
      type: 'number',
    }
  },
  dto: {
    create: CreateScreenDto,
    update: UpdateScreenDto,
  },
  routes: {
    exclude: [
      'replaceOneBase',
      'recoverOneBase'
    ],
    createOneBase: {
      decorators: [UseGuards(IsOwnerEventGuard)]
    },
    createManyBase: {
      decorators: [UseGuards(IsOwnerEventGuard)]
    },
    updateOneBase: {
      decorators: [UseGuards(IsOwnerScreenResourceGuard)]
    },
    deleteOneBase: {
      decorators: [UseGuards(IsOwnerScreenResourceGuard)]
    }
  }
})
@CrudAuth({
  property: 'user',
  persist: (user: User) => ({
    userId: user.id
  }),
})
@Controller('/events/:eventId/screens')
export class ScreensController implements CrudController<Screen> {
  constructor(public service: ScreensService) {}
}
