import { Base } from "src/common/entities/base.entity";
import { Event } from "src/events/entities/event.entity";
import { User } from "src/users/entities/user.entity";
import { Column, Entity, ManyToOne } from "typeorm";


@Entity({ name: 'screens' })
export class Screen extends Base {
  @Column()
  name: string;

  @Column()
  eventId: Event['id'];

  @Column()
  userId: User['id'];

  // relations
  @ManyToOne(() => Event, event => event.screens, {
    onDelete: 'CASCADE'
  })
  event: Event;

  @ManyToOne(() => User, user => user.screens, {
    onDelete: 'CASCADE'
  })
  user: User;
}