import { ApiProperty } from "@nestjsx/crud/lib/crud";
import { IsDefined, IsNumber } from "class-validator";
import { Content } from "src/content/entities/content.entity";
import { Playlist } from "src/playlists/entities/playlist.entity";
import { Column } from "typeorm";


export class CreatePlaylistContentDto {
  @ApiProperty()
  @IsNumber()
  @IsDefined()
  duration: number;

  @ApiProperty()
  @IsNumber()
  @IsDefined()
  ordinalNumber: number;

  @ApiProperty()
  @IsNumber()
  @IsDefined()
  contentId: Content['id'];

  @ApiProperty()
  @IsNumber()
  @IsDefined()
  playlistId: Playlist['id'];
}