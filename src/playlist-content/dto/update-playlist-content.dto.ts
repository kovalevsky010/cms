import { ApiProperty } from "@nestjsx/crud/lib/crud";
import { IsNumber, IsOptional } from "class-validator";
import { Content } from "src/content/entities/content.entity";
import { Playlist } from "src/playlists/entities/playlist.entity";
import { Column } from "typeorm";


export class UpdatePlaylistContentDto {
  @ApiProperty()
  @IsNumber()
  @IsOptional()
  duration?: number;

  @ApiProperty()
  @IsNumber()
  @IsOptional()
  ordinalNumber?: number;

  @ApiProperty()
  @IsNumber()
  @IsOptional()
  contentId?: Content['id'];

  @ApiProperty()
  @IsNumber()
  @IsOptional()
  playlistId?: Playlist['id'];
}
