import { Content } from "src/content/entities/content.entity";
import { Playlist } from "src/playlists/entities/playlist.entity";
import { Column, Entity, ManyToOne, PrimaryColumn } from "typeorm";


@Entity({name: 'playlist_content'})
export class PlaylistContent {
  @PrimaryColumn()
  contentId: Content['id'];

  @PrimaryColumn()
  playlistId: Playlist['id'];

  @Column()
  duration: number;

  @Column()
  ordinalNumber: number;

  // relations
  @ManyToOne(() => Content, content => content.playlistContent, {
    onDelete: 'CASCADE'
  })
  content: Content;

  @ManyToOne(() => Playlist, playlist => playlist.playlistContent, {
    onDelete: 'CASCADE'
  })
  playlist: Playlist;
}