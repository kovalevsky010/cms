import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Content} from 'src/content/entities/content.entity';
import { Playlist } from 'src/playlists/entities/playlist.entity';
import { PlaylistContentController } from './playlist-content.controller';
import { PlaylistContent } from './entities/playlist-content.entity';
import { PlaylistContentService } from './playlist-content.service';
import { PlaylistContentRepository } from './playlist-content.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      PlaylistContent,
      Playlist,
      Content,
      PlaylistContentRepository,
    ])
  ],
  providers: [PlaylistContentService],
  controllers: [PlaylistContentController]
})
export class PlaylistContentModule {}
