import { Controller, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { Crud, CrudAuth, CrudController } from '@nestjsx/crud';
import { CreatePlaylistContentDto } from './dto/create-playlist-content.dto';
import { PlaylistContent } from './entities/playlist-content.entity';
import { PlaylistContentService } from './playlist-content.service';
import { UpdatePlaylistContentDto } from './dto/update-playlist-content.dto';
import { User } from 'src/users/entities/user.entity';
import { IsOwnerContentGuard, IsOwnerPlaylistGuard } from 'src/common/guards/parent/is-owner-parent.guard';

@ApiTags('playlist-content')
@ApiBearerAuth('JWT')
@ApiUnauthorizedResponse({
  description: 'Unauthorized'
})
@Crud({
  model: {
    type: PlaylistContent
  },
  params: {
    playlistId: {
      field: 'playlistId',
      type: 'number',
      primary: true
    },
    contentId: {
      field: 'contentId',
      type: 'number',
      primary: true
    }
  },
  dto: {
    create: CreatePlaylistContentDto,
    update: UpdatePlaylistContentDto
  },
  query: {
    join: {
      content: {
        eager: true
      },
    },
  },
  routes: {
    exclude: [
      'replaceOneBase',
      'recoverOneBase',
      'createManyBase',
    ],
    createOneBase: {
      decorators: [
        UseGuards(IsOwnerPlaylistGuard),
        UseGuards(IsOwnerContentGuard),
      ],
    },
    updateOneBase: {
      decorators: [
        UseGuards(IsOwnerPlaylistGuard),
        UseGuards(IsOwnerContentGuard),
      ],
    },
    deleteOneBase: {
      decorators: [
        UseGuards(IsOwnerPlaylistGuard),
        UseGuards(IsOwnerContentGuard),
      ],
    },
  },
})
@CrudAuth({
  property: 'user',
  persist: (user: User) => ({
    userId: user.id,
  })
})
@Controller('/playlist-content')
export class PlaylistContentController implements CrudController<PlaylistContent> {
  constructor(public service: PlaylistContentService) {}
}
