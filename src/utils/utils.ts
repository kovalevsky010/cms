export function getRandomEnumValue<T>(anEnum: T): T[keyof T] {
  const enumValues = Object.keys(anEnum) as Array<keyof T>; 
  const randomIndex = Math.floor(Math.random() * enumValues.length);
  const randomEnumKey = enumValues[randomIndex];
  return anEnum[randomEnumKey];
}
